function demo(numSuperpixel, labEdgeWeight, dispEdgeWeight, appendix)

%execSGBM = '~/repos/boundarySLIC/cpp/libraries/SGBM/stereo_match';
%paramsSGBM = '--algorithm=sgbm --max-disparity=256 --blocksize=3 --no-display';
%paramsSGBM_ = '--algorithm=sgbm --min-disparity=-256 --max-disparity=256 --blocksize=3 --no-display';
%execSGBM = '~/repos/boundarySLIC/cpp/libraries/sgmstereo/sgmstereo';
execStereoSLIC = 'cpp/stereoslic';
paramsStereoSLIC = sprintf('-s %d -c 3000 -d 100 -p 3 -i 10', numSuperpixel);
paramsBoundarySLIC = sprintf('-s %d -c 3000 -d 100 -p 3 -i 10 -b 1 -e %d -w %d', numSuperpixel, labEdgeWeight, dispEdgeWeight);
LEFT_IMAGE_PATH = fullfile('data/KITTI_stereo', 'training', 'image_0');
RIGHT_IMAGE_PATH = fullfile('data/KITTI_stereo', 'training', 'image_1');
LEFT_DISP_PATH = fullfile('data/KITTI_stereo', 'training', 'leftDisparity');
RIGHT_DISP_PATH = fullfile('data/KITTI_stereo', 'training', 'rightDisparity');
RESULT_STEREO_SLIC_TRAIN = fullfile('data/KITTI_stereo', 'training', 'results', sprintf('stereoSLIC_s%d', numSuperpixel));
RESULT_BOUNDARY_SLIC_TRAIN = fullfile('data/KITTI_stereo', 'training', 'results', sprintf(['boundarySLIC_s%d_e%d_w%.5f_' appendix], numSuperpixel, labEdgeWeight, dispEdgeWeight));

if ~exist(LEFT_DISP_PATH, 'dir')
    mkdir(LEFT_DISP_PATH);
end

if ~exist(RIGHT_DISP_PATH, 'dir')
    mkdir(RIGHT_DISP_PATH);
end

if ~exist(RESULT_STEREO_SLIC_TRAIN, 'dir')
    mkdir(RESULT_STEREO_SLIC_TRAIN);
end

if ~exist(RESULT_BOUNDARY_SLIC_TRAIN, 'dir')
    mkdir(RESULT_BOUNDARY_SLIC_TRAIN);
end

for i = 0:193
    imname = sprintf('%06d_10.png', i);
    left_disparity = strrep(imname, '.png', '_left_disparity.png');
    right_disparity = strrep(imname, '.png', '_right_disparity.png');
%     cmd = [execSGBM, ' ', fullfile(LEFT_IMAGE_PATH, imname), ' ', ...
%            fullfile(RIGHT_IMAGE_PATH, imname), ' -o ' LEFT_DISP_PATH];
%     system(cmd);
    %%% Generate the disparity using SGBM
%     disp_outFileName = strrep(imname, '.png', 'D.png');
%     if ~exist(fullfile(LEFT_DISP_PATH, disp_outFileName), 'file')
%         cmd1 = [execSGBM, ' ', fullfile(LEFT_IMAGE_PATH, imname), ' ', fullfile(RIGHT_IMAGE_PATH,...
%                imname), ' ', paramsSGBM, ' -o ', fullfile(LEFT_DISP_PATH, disp_outFileName)];
%         system(cmd1);
%     end
%     if ~exist(fullfile(RIGHT_DISP_PATH, disp_outFileName), 'file')
%         cmd2 = [execSGBM, ' ', fullfile(RIGHT_IMAGE_PATH, imname), ' ', fullfile(LEFT_IMAGE_PATH,...
%                imname), ' ', paramsSGBM_, ' -o ', fullfile(RIGHT_DISP_PATH, disp_outFileName)];
%         system(cmd2);
%     end
    
    %%% stereoSLIC
    if ~exist(fullfile(RESULT_STEREO_SLIC_TRAIN, imname), 'file')
        cmd = [execStereoSLIC, ' ', fullfile(LEFT_IMAGE_PATH, imname), ' ', ...
               fullfile(LEFT_DISP_PATH, left_disparity), ' ', fullfile(RIGHT_DISP_PATH,...
               right_disparity), ' ', fullfile(RIGHT_IMAGE_PATH, imname), ' ', paramsStereoSLIC,...
               ' -o ', fullfile(RESULT_STEREO_SLIC_TRAIN, imname)];
        system(cmd);
    end
    
    %%% boundarySLIC
    if ~exist(fullfile(RESULT_BOUNDARY_SLIC_TRAIN, imname), 'file')
        cmd = [execStereoSLIC, ' ', fullfile(LEFT_IMAGE_PATH, imname), ' ', ...
               fullfile(LEFT_DISP_PATH, left_disparity), ' ', fullfile(RIGHT_DISP_PATH,...
               right_disparity), ' ', fullfile(RIGHT_IMAGE_PATH, imname), ' ', paramsBoundarySLIC,...
               ' -o ', fullfile(RESULT_BOUNDARY_SLIC_TRAIN, imname)];
        system(cmd);
    end
end
