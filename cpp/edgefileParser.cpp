#include "edgefileParser.h"
#include "zlib.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>

edgefileParser::edgefileParser(std::string fileName):appendix(fileName) {
   
    FILE* matFile = fopen(appendix.c_str(), "r");
    if (matFile == NULL) {
	std::cout << "Unable to open the edgeMap file" << std::endl;
	exit(0);
    }
    z_stream matStream;
    matStream.zalloc = Z_NULL;
    matStream.zfree = Z_NULL;
    matStream.opaque = Z_NULL;
    matStream.avail_in = 0;
    matStream.next_in = Z_NULL;
    int ok = inflateInit(&matStream);
    if (ok != Z_OK)
	exit(0);
    
    fseek(matFile, 132, SEEK_SET); //132 = 128 bytes file header + 4 bytes data type 
    int sizeOfCompressedData[1];	
    size_t result = fread(sizeOfCompressedData, 4, 1, matFile);
    if (result != 1) {
	std::cout << "Error in reading edgeMap files???" << std::endl;
    }
    unsigned char* data = new unsigned char[sizeOfCompressedData[0]];
    result = fread(data, 1, sizeOfCompressedData[0], matFile);
    if (result != sizeOfCompressedData[0]) {
	std::cout << "Error in reading edgeMap files???" << std::endl;
    }

    char buf[8];
    matStream.avail_in = sizeOfCompressedData[0];
    matStream.next_in = (unsigned char*)&(data[0]);
    matStream.avail_out = 8;
    matStream.next_out = (unsigned char*)&buf;
    ok = inflate(&matStream, Z_NO_FLUSH);
    if (ok != Z_OK)
        std::cout << "Unable to inflate the header" << std::endl;

    int uncompressedDataSize = *(int*)&buf[4];
    char* uncompressedData = new char[uncompressedDataSize];
    matStream.avail_out = uncompressedDataSize;
    matStream.next_out = (unsigned char*) uncompressedData;
    inflate(&matStream, Z_FINISH);
    ok = inflateEnd(&matStream);
    if (ok != Z_OK) {
	std::cout << "Error in parsing" << std::endl;
    }
	
    height = *(int*)&uncompressedData[24];
    width = *(int*)&uncompressedData[28];
    int numOfElements = *(int*)(&uncompressedData[44]);
    if (numOfElements/8 != height*width) {
        std::cout << "Error in parsing the Mat file!" << std::endl;
 	exit(0);
    }

    double* ptr = (double*)(&uncompressedData[48]);
    edgeMap.resize(height*width, 0);
    int ind = 0;
    for (int i = 0; i < height; i++) {
	for (int j= 0; j < width; j++) {
           edgeMap[ind] = ptr[j*height+i];
	   ind = ind + 1;
        }
    }  

    delete[] uncompressedData;
    delete[] data;

    
    fclose(matFile);
}

int edgefileParser::getHeight() {
    return height;
}

int edgefileParser::getWidth() {
    return width;
}

std::vector<double>& edgefileParser::getEdgeMap() {
    return edgeMap;
}
