#ifndef POINT_ARRAY_H
#define POINT_ARRAY_H

#include <cmath>
class pointArray {
private:
    int * point_ptr;
    int numPoints;
public:
    pointArray(int x0, int y0, int x1, int y1);
    pointArray(const pointArray& object);
    ~pointArray();
    int* getArrayPointer();
    int getArrayNumber();
};
#endif
