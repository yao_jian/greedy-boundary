#include "pointArray.h"

//pointArray::pointArray(){};
pointArray::pointArray(int x0, int y0, int x1, int y1)
{
/*
    const int* point0; const int* point1;
    int x0 = point0[0];
    int y0 = point0[1];
    int x1 = point1[0];
    int y1 = point1[1];
*/    
    bool steep = (std::abs(y1-y0) > std::abs(x1-x0));
    if (steep)
    {
        int temp = x0;
        x0 = y0;
        y0 = temp;
        
        temp = x1;
        x1 = y1;
        y1 = temp;
    }
    
    int deltax = std::abs(x1-x0);
    int deltay = std::abs(y1-y0);
    int error = deltax/2;
    int ystep;
    int y = y0;
    
    if (y0<y1)
        ystep = 1;
    else
        ystep = -1;
    
    int inc;
    if (x0<x1)
        inc = 1;
    else
        inc = -1;
    
    point_ptr = new int[(deltax+1)*2];
    int accumulator = 0;
    int x = x0;
    while (accumulator < deltax+1)
    {
        if (steep)
        {
            point_ptr[accumulator] = y;
            point_ptr[accumulator+deltax+1] = x;
        }
        else
        {
            point_ptr[accumulator] = x;
            point_ptr[accumulator+deltax+1] = y;
        }
        error = error - deltay;
        if (error<0)
        {
            y = y + ystep;
            error = error + deltax;
        }
        x = x + inc;
        accumulator = accumulator + 1;  
    }
    
    numPoints = deltax+1;        
}
    

pointArray::pointArray(const pointArray& object):numPoints(object.numPoints){
    int* p = new int[numPoints*2];
    for (int i = 0; i<object.numPoints; i++){
        point_ptr[i] = object.point_ptr[i];
        point_ptr[i+numPoints] = object.point_ptr[i+numPoints];
    }
} 

pointArray::~pointArray(){
    delete[] point_ptr;
}	

int* pointArray::getArrayPointer() {
    return(point_ptr);
}

int pointArray::getArrayNumber() {
    return(numPoints);
}
