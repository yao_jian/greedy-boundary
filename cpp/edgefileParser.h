#ifndef EDGE_FILE_PARSER_H
#define EDGE_FILE_PARSER_H

#include <string>
#include <vector>
class edgefileParser {
    private:
	int height;
	int width;
        std::string appendix;
	std::vector<double> edgeMap;
    public:
	edgefileParser(std::string fileName);
	int getHeight();
	int getWidth();
	std::vector<double> & getEdgeMap();
};



#endif
