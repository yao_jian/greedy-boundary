#include <iostream>
#include <png++/png.hpp>
#include "cmdline.h"
#include "StereoSlic.h"

struct ParameterStereoSlic {
    bool verbose;
    bool boundarySLIC;
    int energyType;
    int superpixelTotal;
    int iterationTotal;
    double colorWeight;
    double disparityWeight;
    double noDisparityPenalty;
    double labEdgeWeight;
    double dispEdgeWeight;
    std::string leftImageFilename;
    std::string leftImageEdgeMapFilename;
    std::string rightImageFilename;
    std::string leftDisparityImageFilename;
    std::string rightDisparityImageFilename;
    std::string outputSegmentImageFilename;
    std::string outputDisparityImageFilename;
    std::string outputBoundaryImageFilename;
    std::string outputPlaneParamsFilename;
};

// Prototype declaration
cmdline::parser makeCommandParser();
ParameterStereoSlic parseCommandline(int argc, char* argv[]);

cmdline::parser makeCommandParser() {
    cmdline::parser commandParser;
    commandParser.add<std::string>("output", 'o', "output segment file", false, "");
    commandParser.add<std::string>("edgeMap", 'm', "edgeMap file", false, "");
    commandParser.add<int>("superpixel", 's', "the number of superpixels", false, 400);
    commandParser.add<int>("iteration", 'i', "the number of iterations", false, 10);
    commandParser.add<double>("color", 'c', "weight of color term", false, 3000);
    commandParser.add<double>("disparity", 'd', "weight of disparity term", false, 100);
    commandParser.add<double>("penalty", 'p', "penalty value of disparity term without disparity estimation", false, 3.0);
    commandParser.add<bool>("boundarySLIC", 'b', "extend to the boundarySLIC", false, 0);
    commandParser.add<double>("labEdgeWeight", 'e', "weight of lab boundary term", false, 1);
    commandParser.add<double>("dispEdgeWeight", 'w', "weight of disparity boundary term", false, 0);
    commandParser.add("verbose", 'v', "verbose");
    commandParser.add("help", 'h', "display this message");
    commandParser.footer("left_image [left_disparity_image [right_disparity_image [right_image]]]");
    commandParser.set_program_name("stereoslic");
    
    return commandParser;
}

ParameterStereoSlic parseCommandline(int argc, char* argv[]) {
    // Make command parser
    cmdline::parser commandParser = makeCommandParser();
    // Parse command line
    bool isCorrectCommandline = commandParser.parse(argc, argv);
    // Check arguments
    if (!isCorrectCommandline) {
        std::cerr << commandParser.error() << std::endl;
    }
    if (!isCorrectCommandline || commandParser.exist("help") || commandParser.rest().size() < 1) {
        std::cerr << commandParser.usage() << std::endl;
        exit(1);
    }
    
    // Set program parameters
    ParameterStereoSlic parameters;
    // Verbose flag
    parameters.verbose = commandParser.exist("verbose");
    // Type of energy function
    if (commandParser.rest().size() > 3) parameters.energyType = 3;
    else if (commandParser.rest().size() > 2) parameters.energyType = 2;
    else if (commandParser.rest().size() > 1) parameters.energyType = 1;
    else parameters.energyType = 0;
    // Decide whether to use boundarySLIC or not
    parameters.boundarySLIC = commandParser.get<bool>("boundarySLIC");
    // The number of superpixels
    parameters.superpixelTotal = commandParser.get<int>("superpixel");
    // The number of iterations
    parameters.iterationTotal = commandParser.get<int>("iteration");
    // Color weight
    parameters.colorWeight = commandParser.get<double>("color");
    // Disparity weight
    parameters.disparityWeight = commandParser.get<double>("disparity");
    // Occluded penalty
    parameters.noDisparityPenalty = commandParser.get<double>("penalty");
    // Edge Weight
    parameters.labEdgeWeight = commandParser.get<double>("labEdgeWeight");
    parameters.dispEdgeWeight = commandParser.get<double>("dispEdgeWeight");
    // Input files
    parameters.leftImageFilename = commandParser.rest()[0];
    parameters.leftImageEdgeMapFilename = commandParser.get<std::string>("edgeMap");
    if (parameters.energyType > 0) parameters.leftDisparityImageFilename = commandParser.rest()[1];
    else parameters.leftDisparityImageFilename = "";
    if (parameters.energyType > 1) parameters.rightDisparityImageFilename = commandParser.rest()[2];
    else parameters.rightDisparityImageFilename = "";
    if (parameters.energyType > 2) parameters.rightImageFilename = commandParser.rest()[3];
    else parameters.rightImageFilename = "";
    // Output files
    std::string outputSegmentImageFilename = commandParser.get<std::string>("output");
    if (outputSegmentImageFilename == "") {
        outputSegmentImageFilename = parameters.leftImageFilename;
        size_t dotPosition = outputSegmentImageFilename.rfind('.');
        if (dotPosition != std::string::npos) outputSegmentImageFilename.erase(dotPosition);
        outputSegmentImageFilename += "_slic.png";
    }
    parameters.outputSegmentImageFilename = outputSegmentImageFilename;
    std::string outputDisparityImageFilename = outputSegmentImageFilename;
    size_t dotPosition = outputDisparityImageFilename.rfind('.');
    if (dotPosition != std::string::npos) outputDisparityImageFilename.erase(dotPosition);
    outputDisparityImageFilename += "_disparity.png";
    parameters.outputDisparityImageFilename = outputDisparityImageFilename;
    std::string outputBoundaryImageFilename = outputSegmentImageFilename;
    dotPosition = outputBoundaryImageFilename.rfind('.');
    if (dotPosition != std::string::npos) outputBoundaryImageFilename.erase(dotPosition);
    outputBoundaryImageFilename += "_boundary.png";
    parameters.outputBoundaryImageFilename = outputBoundaryImageFilename;
    std::string outputPlaneParamsFilename = outputSegmentImageFilename;
    dotPosition = outputPlaneParamsFilename.rfind('.');
    if (dotPosition != std::string::npos) outputPlaneParamsFilename.erase(dotPosition);
    outputPlaneParamsFilename += "_plane_params.txt";
    parameters.outputPlaneParamsFilename = outputPlaneParamsFilename;
    
    return parameters;
}

int main(int argc, char* argv[]) {
    // Parse command line
    ParameterStereoSlic parameters = parseCommandline(argc, argv);

    if (parameters.verbose) {
        std::cerr << std::endl;
        if (parameters.boundarySLIC)
	    std::cerr << "This is boundarySLIC" << std::endl;
        std::cerr << "Left image:             " << parameters.leftImageFilename << std::endl;
        if (parameters.energyType > 0) std::cerr << "Left disparity image:   " << parameters.leftDisparityImageFilename << std::endl;
        if (parameters.energyType > 1) std::cerr << "Right image:            " << parameters.rightImageFilename << std::endl;
        if (parameters.energyType > 2) std::cerr << "Right disparity image:  " << parameters.rightDisparityImageFilename << std::endl;
        std::cerr << "Output segment image:   " << parameters.outputSegmentImageFilename << std::endl;
        if (parameters.energyType > 0) std::cerr << "Output disparity image: " << parameters.outputDisparityImageFilename << std::endl;
        std::cerr << "Output boundary image:  " << parameters.outputBoundaryImageFilename << std::endl;
        std::cerr << "   Energy type:      ";
        if (parameters.energyType == 0) std::cerr << "color(left)" << std::endl;
        else if (parameters.energyType == 1) std::cerr << "color(left) + disparity(left)" << std::endl;
        else if (parameters.energyType == 2) std::cerr << "color(left) + disparity(left/right)" << std::endl;
        else std::cerr << "color(left/right) + disparity(left/right)" << std::endl;
        std::cerr << "   #superpixels:     " << parameters.superpixelTotal << std::endl;
        std::cerr << "   #iterations:      " << parameters.iterationTotal << std::endl;
        std::cerr << "   color weight:     " << parameters.colorWeight << std::endl;
        std::cerr << "   dispairty weight: " << parameters.disparityWeight << std::endl;
        std::cerr << "   no data penalty:  " << parameters.noDisparityPenalty << std::endl;
	std::cerr << "   boundarySLIC:     " << parameters.boundarySLIC << std::endl;
        std::cerr << "   lab edge weight:  " << parameters.labEdgeWeight << std::endl;
        std::cerr << "disparity edge weight:" << parameters.dispEdgeWeight << std::endl;
        std::cerr << std::endl;
    }
    
    try {
        StereoSlic stereoSlic;
        stereoSlic.setIterationTotal(parameters.iterationTotal);
        stereoSlic.setEnergyParameter(parameters.colorWeight, parameters.disparityWeight, parameters.noDisparityPenalty);
	stereoSlic.setBoundarySlicParameter(parameters.boundarySLIC, parameters.labEdgeWeight, parameters.dispEdgeWeight, parameters.leftImageEdgeMapFilename);
        
        png::image<png::gray_pixel_16> segmentImage;
        png::image<png::gray_pixel_16> disparityImage;
        
        png::image<png::rgb_pixel> leftImage(parameters.leftImageFilename);
        if (parameters.energyType == 0) {
            stereoSlic.segment(parameters.superpixelTotal, leftImage, segmentImage, parameters.outputPlaneParamsFilename);
        } else if (parameters.energyType == 1) {
            png::image<png::gray_pixel_16> leftDisparityImage(parameters.leftDisparityImageFilename);
            stereoSlic.segment(parameters.superpixelTotal, leftImage, leftDisparityImage, segmentImage, disparityImage, parameters.outputPlaneParamsFilename);
        } else if (parameters.energyType == 2) {
            png::image<png::gray_pixel_16> leftDisparityImage(parameters.leftDisparityImageFilename);
            png::image<png::gray_pixel_16> rightDisparityImage(parameters.rightDisparityImageFilename);
            stereoSlic.segment(parameters.superpixelTotal, leftImage, leftDisparityImage, rightDisparityImage, segmentImage, disparityImage, parameters.outputPlaneParamsFilename);
        } else {
            png::image<png::gray_pixel_16> leftDisparityImage(parameters.leftDisparityImageFilename);
            png::image<png::gray_pixel_16> rightDisparityImage(parameters.rightDisparityImageFilename);
            png::image<png::rgb_pixel> rightImage(parameters.rightImageFilename);
            stereoSlic.segment(parameters.superpixelTotal,
                               leftImage, leftDisparityImage,
                               rightDisparityImage, rightImage,
                               segmentImage, disparityImage, parameters.outputPlaneParamsFilename);
        }

        // write the resulting images: segment image, disparity image, boundary image
        png::image<png::rgb_pixel> boundaryImage = drawSegmentBoundary(leftImage, segmentImage);
        boundaryImage.write(parameters.outputBoundaryImageFilename);
        
        segmentImage.write(parameters.outputSegmentImageFilename);
        if (parameters.energyType > 0) disparityImage.write(parameters.outputDisparityImageFilename);

        /*
	png::image<png::rgb_pixel> rgbDisp(disparityImage.get_width(), disparityImage.get_height());
	for (int y=0; y<disparityImage.get_height(); y++) {
	    for (int x=0; x<disparityImage.get_width(); x++) {
		int rgbValue = (int)disparityImage[y][x]/256+0.5;
		rgbDisp[y][x] = png::rgb_pixel(rgbValue, rgbValue, rgbValue);
	    }
	}
        png::image<png::rgb_pixel> boundaryOverDisp = drawSegmentBoundary(rgbDisp, segmentImage);
        boundaryOverDisp.write("foo.png");
	*/

    } catch (const std::exception& exception) {
        std::cerr << "Error: " << exception.what() << std::endl;
        exit(1);
    }
}
