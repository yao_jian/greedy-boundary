function [USsup, BRsup, ASAsup] = testingSeeds(numSuperpixelArray)

GT_PATH = 'BSR/BSDS500/data/groundTruth/test';
IMAGE_PATH = 'BSR/BSDS500/data/images/test';
RESULT_PATH = 'label_seeds';

imlist = dir(fullfile(IMAGE_PATH, '*.png'));

US = zeros(1, length(imlist)); 
BR = zeros(1, length(imlist)); 
ASA = zeros(1, length(imlist));

USsup = zeros(1, length(numSuperpixelArray));
BRsup = zeros(1, length(numSuperpixelArray));
ASAsup = zeros(1, length(numSuperpixelArray));

for i = 1:length(numSuperpixelArray)
    numSup = numSuperpixelArray(i);
    for j = 1:length(imlist)
        if mod(j, 50)==0
            fprintf('%d images processed!\n', j);
        end
        imname = imlist(j).name;
        I = imread(fullfile(IMAGE_PATH, imname));
        width = size(I,2);
        height = size(I,1);    
        fid = fopen(fullfile(RESULT_PATH, [imname(1:end-4), '_', num2str(numSup), '_labels.txt']));
        T = textscan(fid, '%u');
        T = T{1};
        resultLabel = reshape(T, width, height)';
        gtFile = load(fullfile(GT_PATH, [imname(1:end-4) '.mat']));
        gt = gtFile.groundTruth;
        acc = evaluatePerformance(gt, resultLabel);
        US(j) = acc.underSeg;
        BR(j) = acc.boundaryRecall;
        ASA(j) = acc.ASA;
    end
    USsup(i) = mean(US);
    BRsup(i) = mean(BR);
    ASAsup(i) = mean(ASA);

end