function assignment = assignLabel(ILab, LabEdges, seeds, assignment, superpixelTotal, edgeWeight)

GlobalVar;
labels = assignment.labels;
distancesToSeeds = assignment.distancesToSeeds;

seedTotal = length(seeds);
height = size(ILab, 1);
width = size(ILab, 2);
gridSize = sqrt(height*width/superpixelTotal);
stepSize = floor(gridSize + 2);
positionWeight = positionWeight/(stepSize*stepSize); %#ok<NODEF>


for i = 1:seedTotal
    if seeds(4,i) > stepSize+1
        minX = floor(seeds(4,i) - stepSize);
    else
        minX = 1;
    end
    if seeds(4,i) + stepSize < width + 1
        maxX = floor(seeds(4,i) + stepSize)-1;
    else
        maxX = width;
    end
    if seeds(5,i) > stepSize+1
        minY = floor(seeds(5,i) - stepSize);
    else
        minY = 1;
    end
    if seeds(5,i) + stepSize < height+1
        maxY = floor(seeds(5,i) + stepSize)-1;
    else
        maxY = height;
    end
    
    Ipatch = ILab(minY:maxY, minX:maxX, :);
    LabSeed = reshape(seeds(1:3, i), [1, 1, 3]);
    distanceLab = Ipatch - repmat(LabSeed, [size(Ipatch,1), size(Ipatch,2), 1]);
    distanceLab = sum(distanceLab.^2,3); 
    
    xIpatch = repmat(minX:maxX, [size(Ipatch,1), 1]);
    yIpatch = repmat((minY:maxY)', [1, size(Ipatch,2)]);
    dx = seeds(4,i)*ones(size(Ipatch,1), size(Ipatch,2)) - xIpatch;
    dy = seeds(5,i)*ones(size(Ipatch,1),size(Ipatch,2)) - yIpatch;
    distanceXY = dx.*dx + dy.*dy;
    
    distanceLabXY = distanceLab + positionWeight*distanceXY;
    if FEAT_EDGE == 1
        distanceEdge = getPoints(xIpatch-1, yIpatch-1, seeds(4:5,i)'-1, LabEdges);
        distanceTotal = distanceLabXY + edgeWeight*distanceEdge;
    else
        distanceTotal = distanceLabXY;          
    end
    miniLabel = labels(minY:maxY, minX:maxX);
    miniDistToSeeds = distancesToSeeds(minY:maxY, minX:maxX);
    updatedIndex = miniDistToSeeds-distanceTotal > 0;
    miniLabel(updatedIndex) = i;
    labels(minY:maxY, minX:maxX) = miniLabel;
    miniDistToSeeds(updatedIndex) = distanceTotal(updatedIndex); 
    distancesToSeeds(minY:maxY, minX:maxX) = miniDistToSeeds;
end

assignment.labels = labels;
assignment.distancesToSeeds = distancesToSeeds;









