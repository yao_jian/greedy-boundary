function drawEvaluationCurve()

close all;

methodTypes = {'SLIC', 'SEEDS', 'boundarySLIC'};
%metricTypes = {'US', 'BR', 'ASA'};
numSuperpixelArray = [50, 100, 200, 300, 400];

US = zeros(length(methodTypes), 5);
BR = US;
ASA = US;
for i = 1:length(methodTypes)
    T = load([methodTypes{i} '_BSD.mat']);
    US(i,:) = T.USsup;
    BR(i,:) = T.BRsup;
    ASA(i,:) = T.ASAsup;
end
US_ = US(1:2,:);
BR_ = BR(1:2,:);
ASA_ = ASA(1:2,:);

figure(1);
axis([50, 400, 0, 3]);
plot(numSuperpixelArray, US(1,:), 'g-s');
hold on;
plot(numSuperpixelArray, US(2,:), 'c-s');
hold on;
plot(numSuperpixelArray, US(3,:), 'r-s');
title('Undersegmentation Error');
xlabel('number of superpixels');
ylabel('undersegmentation error');
legend('SLIC', 'SEEDS', 'boundarySLIC');

figure(2);
axis([50, 400, 0.2, 1]);
plot(numSuperpixelArray, BR(1,:), 'g-s');
hold on;
plot(numSuperpixelArray, BR(2,:), 'c-s');
hold on;
plot(numSuperpixelArray, BR(3,:), 'r-s');
title('Boundary Recall');
xlabel('Number of Superpixels');
ylabel('Boundary Recall');
legend('SLIC', 'SEEDS', 'boundarySLIC');

figure(3);
axis([50, 400, 0.8, 1]);
plot(numSuperpixelArray, ASA(1,:), 'g-s');
hold on;
plot(numSuperpixelArray, ASA(2,:), 'c-s');
hold on;
plot(numSuperpixelArray, ASA(3,:), 'r-s');
title('Achievable Segmentation Accuracy');
xlabel('Number of Superpixels');
ylabel('Achievable Segmentation Accuracy');
legend('SLIC', 'SEEDS', 'boundarySLIC');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
edgeWeight = [1 2 5 10 20 50];
US = zeros(length(edgeWeight), 5);
BR = US;
ASA = US;
for i = 1:length(edgeWeight)
    T = load(sprintf('../data/result/acc_BSD_w%d.mat', edgeWeight(i)));
    US(i,:) = T.USsup;
    BR(i,:) = T.BRsup;
    ASA(i,:) = T.ASAsup;
end
US = [US; US_];
BR = [BR; BR_];
ASA = [ASA; ASA_];

figure;
axis([50, 400, 0, 3]);
plot(numSuperpixelArray, US(1,:), 'g-s');
hold on;
plot(numSuperpixelArray, US(2,:), 'c-s');
hold on;
plot(numSuperpixelArray, US(3,:), 'r-s');
hold on;
plot(numSuperpixelArray, US(4,:), 'm-s');
hold on;
plot(numSuperpixelArray, US(5,:), 'k-s');
hold on;
plot(numSuperpixelArray, US(6,:), 'b-s');
hold on;
plot(numSuperpixelArray, US(7,:), 'y-s');
hold on;
plot(numSuperpixelArray, US(8,:), '-s', 'Color', [0.5 0.5 0.5]);
title('Undersegmentation Error');
xlabel('number of superpixels');
ylabel('undersegmentation error');
legend('w=1', 'w=2', 'w=5', 'w=10', 'w=20', 'w=50', 'originSLIC', 'SEEDS');

figure;
axis([50, 400, 0.2, 1]);
plot(numSuperpixelArray, BR(1,:), 'g-s');
hold on;
plot(numSuperpixelArray, BR(2,:), 'c-s');
hold on;
plot(numSuperpixelArray, BR(3,:), 'r-s');
hold on;
plot(numSuperpixelArray, BR(4,:), 'm-s');
hold on;
plot(numSuperpixelArray, BR(5,:), 'k-s');
hold on;
plot(numSuperpixelArray, BR(6,:), 'b-s');
hold on;
plot(numSuperpixelArray, BR(7,:), 'y-s');
hold on;
plot(numSuperpixelArray, BR(8,:), '-s', 'Color', [0.5 0.5 0.5]);
title('Boundary Recall');
xlabel('Number of Superpixels');
ylabel('Boundary Recall');
legend('w=1', 'w=2', 'w=5', 'w=10', 'w=20', 'w=50', 'originSLIC', 'SEEDS');

figure;
axis([50, 400, 0.8, 1]);
plot(numSuperpixelArray, ASA(1,:), 'g-s');
hold on;
plot(numSuperpixelArray, ASA(2,:), 'c-s');
hold on;
plot(numSuperpixelArray, ASA(3,:), 'r-s');
hold on;
plot(numSuperpixelArray, ASA(4,:), 'm-s');
hold on;
plot(numSuperpixelArray, ASA(5,:), 'k-s');
hold on;
plot(numSuperpixelArray, ASA(6,:), 'b-s');
hold on;
plot(numSuperpixelArray, ASA(7,:), 'y-s');
hold on;
plot(numSuperpixelArray, ASA(8,:), '-s', 'Color', [0.5 0.5 0.5]);
title('Achievable Segmentation Accuracy');
xlabel('Number of Superpixels');
ylabel('Achievable Segmentation Accuracy');
legend('w=1', 'w=2', 'w=5', 'w=10', 'w=20', 'w=50', 'originSLIC', 'SEEDS');




