function [boundaryRecall, boundaryLength] = computeBoundaryRecall(gtLabel, resultLabel, tolerance)

if ~islogical(gtLabel)
    gtBoundary = drawBoundary(gtLabel);
else
    gtBoundary = gtLabel;
end
boundaryTotal = nnz(gtBoundary);
resultBoundary = drawBoundary(resultLabel);
boundaryLength = nnz(resultBoundary);
gtBoundary = single(gtBoundary);
swapM = single(resultBoundary);
swapM(resultBoundary) = 2;
resultBoundary = swapM;

eightNeighborOffsetX = [-1 -1 0 1 1 1 0 -1];
eightNeighborOffsetY = [0 -1 -1 -1 0 1 1 1];
width = size(gtLabel, 2);
height = size(gtLabel, 1);
detectedBoundary = 0;

for i = 0:tolerance
    if i == 0
        temp = gtBoundary + resultBoundary;
        firedPixels = find(temp == 3);
        detectedBoundary = detectedBoundary + length(firedPixels);
        gtBoundary(firedPixels) = 0;
        continue;
    end
    for j = 1:length(eightNeighborOffsetX)
        if eightNeighborOffsetX(j) == 1
            gtStartX = 1;
            gtEndX = width-i;
            leftAppend = 0;
            rightAppend = 1;
            resultStartX = 1+i;
            resultEndX = width; 
        elseif eightNeighborOffsetX(j) == -1
            gtStartX = 1+i;
            gtEndX = width;
            leftAppend = 1;
            rightAppend = 0;
            resultStartX = 1;
            resultEndX = width-i;
        elseif eightNeighborOffsetX(j) == 0
            gtStartX = 1;
            gtEndX = width;
            leftAppend = 0;
            rightAppend = 0;
            resultStartX = 1;
            resultEndX = width;
        end
        
        if eightNeighborOffsetY(j) == 1
            gtStartY = 1;
            gtEndY = height-i;
            topAppend = 0;
            bottomAppend = 1;
            resultStartY = 1+i;
            resultEndY = height;
        elseif eightNeighborOffsetY(j) == -1
            gtStartY = 1+i;
            gtEndY = height;
            topAppend = 1;
            bottomAppend = 0;
            resultStartY = 1;
            resultEndY = height-i;
        elseif eightNeighborOffsetY(j) == 0
            gtStartY = 1;
            gtEndY = height;
            topAppend = 0;
            bottomAppend = 0;
            resultStartY = 1;
            resultEndY = height;
        end
        
        temp = gtBoundary(gtStartY:gtEndY, gtStartX:gtEndX) + ...
               resultBoundary(resultStartY:resultEndY, resultStartX:resultEndX);
        fixM = temp;
        if leftAppend
            fixM = [zeros(size(fixM,1),i) fixM];
        elseif rightAppend
            fixM = [fixM zeros(size(fixM,1),i)];
        end
        if topAppend
            fixM = [zeros(i, size(fixM,2)); fixM];  %#ok<*AGROW>
        elseif bottomAppend
            fixM = [fixM; zeros(i, size(fixM,2))];
        end
        firedPixels = find(fixM == 3);
        detectedBoundary = detectedBoundary + length(firedPixels);
        gtBoundary(firedPixels) = 0;
    end

end

boundaryRecall = detectedBoundary/boundaryTotal;


