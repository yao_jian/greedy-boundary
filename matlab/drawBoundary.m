function boundaryMap = drawBoundary(resultLabel, Irgb, visualize)

if nargin == 1
    visualize = 0;
    Irgb = [];
end
eightNeighborOffsetX = [-1 -1 0 1 1 1 0 -1];
eightNeighborOffsetY = [0 -1 -1 -1 0 1 1 1];

width = size(resultLabel,2);
height = size(resultLabel,1);
boundaryMap = false(height, width);

for i = 1:height
    for j = 1:width
        for k = 1:length(eightNeighborOffsetX)
            neighborX = j + eightNeighborOffsetX(k);
            neighborY = i + eightNeighborOffsetY(k);
            if neighborX < 1 || neighborY < 1 || neighborX > width || neighborY > height
                continue;
            end
            if boundaryMap(neighborY,neighborX) 
                continue;
            end
            if resultLabel(neighborY, neighborX) ~= resultLabel(i,j)
                boundaryMap(i,j) = 1;
                if visualize
                    Irgb(i,j,:) = [255 0 0];
                end
                break;
            end
        end
    end
end

if visualize
    imshow(Irgb);
end