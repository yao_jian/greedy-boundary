function assignment = boundarySLIC(Irgb, superpixelTotal, edgeWeight)

% iterationTotal = 10;
% positionWeight = 3000; %The actual position weight has to be rescaled by stepSize
% superpixelTotal = 100;
GlobalVar;
if nargin < 2
    superpixelTotal = 100;
end

if ndims(Irgb) == 2 %#ok<ISMAT>
    newI = repmat(Irgb, [1, 1, 3]);
    Irgb = newI;
end
 
ILab = convertRGBToLab(double(Irgb));
LabEdges = calcLabEdges(ILab);

seeds = initializeSeeds(ILab, LabEdges, superpixelTotal);
assignment.labels = -1*ones(size(ILab,1), size(ILab,2));
assignment.distancesToSeeds = realmax*ones(size(ILab,1), size(ILab,2));
for i = 1:iterationTotal
    assignment = assignLabel(ILab, LabEdges, seeds, assignment, superpixelTotal, edgeWeight);
    seeds = updateSeeds(ILab, seeds, assignment);
end
assignment.labels = enforceConnectivity(assignment.labels, size(seeds, 2));









