function ASA = computeASA(gtLabel, resultLabel)

gtLabel = double(gtLabel);
superpixelSet = unique(resultLabel);
newAssign = zeros(size(gtLabel));

for i = 1:length(superpixelSet)
    superpixelInd = superpixelSet(i);
    firedRegion = resultLabel == superpixelInd;
    firedGT = gtLabel(firedRegion);
    domGT = mode(firedGT);
    newAssign(firedRegion) = domGT;
end

ASA = 1-nnz(newAssign - gtLabel)/(size(gtLabel,1)*size(gtLabel,2));