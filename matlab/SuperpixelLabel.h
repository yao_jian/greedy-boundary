#ifndef _SUPERPIXEL_LABEL_H_
#define _SUPERPIXEL_LABEL_H_

#include <vector>

class SuperpixelLabel
{
    private:
        double*  labels;
        int height;
        int width;
	int seedTotal;
    public:
        SuperpixelLabel(double* inMatrix, int h, int w, int numSeeds);
        SuperpixelLabel(const SuperpixelLabel &object);
        //~SuperpixelLabel();
	SuperpixelLabel & operator=(const SuperpixelLabel& other);
	void enforceConnectivity();
	void labelConnectedPixels(const int x, const int y, const int newLabelIndex, double* newLabels, std::vector<int>& connectedXs, std::vector<int>& connectedY) const;
        const double* getLabels() {return labels;}

};


#endif
