function LabEdges = calcLabEdges(ILab)

gx = [1 0 -1];
gy = gx';

Ix = imfilter(ILab, gx);
Iy = imfilter(ILab, gy);

LabEdges = sum(Ix.*Ix, 3) + sum(Iy.*Iy, 3);

LabEdges(1, 1:end) = 0;
LabEdges(end, 1:end) = 0;
LabEdges(1:end, 1) = 0;
LabEdges(1:end, end) = 0;