#include <mex.h>
#include <matrix.h>
#include <sstream>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])

{   
    const double* rgbImage = mxGetPr(prhs[0]);
    mwSize ndim = mxGetNumberOfDimensions(prhs[0]);
    const mwSize* dimArray = mxGetDimensions(prhs[0]);
    int height = dimArray[0];
    int width = dimArray[1];
    int imageSize = height*width;
    plhs[0] = mxCreateNumericArray(ndim, dimArray, mxDOUBLE_CLASS, mxREAL);
    double* labImage = mxGetPr(plhs[0]);
    
    const int RGB2LABCONVERTER_XYZ_TABLE_SIZE = 1024;
    // CIE standard parameters
    const double epsilon = 0.008856;
    const double kappa = 903.3;
    // Reference white
    const double referenceWhite[3] = {0.950456, 1.0, 1.088754};
    /// Maximum values
    const double maxXYZValues[3] = {0.95047, 1.0, 1.08883};

    std::vector<float> sRGBGammaCorrections(256);
    for (int pixelValue = 0; pixelValue < 256; ++pixelValue) {
        double normalizedValue = pixelValue/255.0;
        double transformedValue = (normalizedValue <= 0.04045) ? normalizedValue/12.92 : pow((normalizedValue+0.055)/1.055, 2.4);
        
        sRGBGammaCorrections[pixelValue] = transformedValue;
    }
    
    int tableSize = RGB2LABCONVERTER_XYZ_TABLE_SIZE;
    std::vector<double> xyzTableIndexCoefficients(3);
    xyzTableIndexCoefficients[0] = (tableSize-1)/maxXYZValues[0];
    xyzTableIndexCoefficients[1] = (tableSize-1)/maxXYZValues[1];
    xyzTableIndexCoefficients[2] = (tableSize-1)/maxXYZValues[2];
    
    std::vector< std::vector<float> > fXYZConversions(3);
    for (int xyzIndex = 0; xyzIndex < 3; ++xyzIndex) {
        fXYZConversions[xyzIndex].resize(tableSize);
        double stepValue = maxXYZValues[xyzIndex]/tableSize;
        for (int tableIndex = 0; tableIndex < tableSize; ++tableIndex) {
            double originalValue = stepValue*tableIndex;
            double normalizedValue = originalValue/referenceWhite[xyzIndex];
            double transformedValue = (normalizedValue > epsilon) ? pow(normalizedValue, 1.0/3.0) : (kappa*normalizedValue + 16.0)/116.0;
            
            fXYZConversions[xyzIndex][tableIndex] = transformedValue;
        }
    }
    
/*
    //write Lab into files May 1, 2014, by Jian
    std::ofstream myfile("Labimage.txt");
    if (!myfile.is_open())
    {
        std::cout << "Error openning file to write Lab image!" << std::endl;
	exit(0);
    }
    /////////////////////////////
*/
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            int red = rgbImage[height*x + y];
	    int green = rgbImage[imageSize + height*x + y]; 
	    int blue = rgbImage[2*imageSize + height*x + y];
            float correctedR = sRGBGammaCorrections[red];
            float correctedG = sRGBGammaCorrections[green];
            float correctedB = sRGBGammaCorrections[blue];
            
            float xyzColor[3];
            xyzColor[0] = correctedR*0.4124564f + correctedG*0.3575761f + correctedB*0.1804375f;
            xyzColor[1] = correctedR*0.2126729f + correctedG*0.7151522f + correctedB*0.0721750f;
            xyzColor[2] = correctedR*0.0193339f + correctedG*0.1191920f + correctedB*0.9503041f;
            
            int tableIndexX = static_cast<int>(xyzColor[0]*xyzTableIndexCoefficients[0] + 0.5);
            int tableIndexY = static_cast<int>(xyzColor[1]*xyzTableIndexCoefficients[1] + 0.5);
            int tableIndexZ = static_cast<int>(xyzColor[2]*xyzTableIndexCoefficients[2] + 0.5);
            
            float fX = fXYZConversions[0][tableIndexX];
            float fY = fXYZConversions[1][tableIndexY];
            float fZ = fXYZConversions[2][tableIndexZ];
            
            labImage[height*x + y] = 116.0*fY - 16.0;
            labImage[imageSize + height*x + y] = 500.0*(fX - fY);
            labImage[2*imageSize + height*x + y] = 200.0*(fY - fZ);

            /////////////////////////////////////////
	    //myfile << labImage[width_*3*y+3*x] << " " << labImage[width_*3*y+3*x+1] << " " << labImage[width_*3*y+3*x+2] << "\n";
            ////////////////////////////////////////
        }
    }
    //myfile.close();
}
