function seeds = updateSeeds(ILab, seeds, assignment)

labels = assignment.labels;
seedTotal = length(seeds);

for i = 1:seedTotal
    updatedLabel = (labels == i);
    [updatedR updatedC] = find(updatedLabel);
    L = ILab(:,:,1);
    A = ILab(:,:,2);
    B = ILab(:,:,3);
    seeds(1,i) = mean(L(updatedLabel));
    seeds(2,i) = mean(A(updatedLabel));
    seeds(3,i) = mean(B(updatedLabel));
    seeds(4:5, i) = [mean(updatedC) mean(updatedR)];
end

% height = size(ILab,1);
% width = size(ILab,2);
% segmentSizes = zeros(seedTotal,1);
% segmentSigmas = zeros(5, seedTotal);
% for i = 1:height
%     for j = 1:width
%         seedID = labels(i,j);
%         if seedID > 0
%             segmentSigmas(1:3, seedID) = segmentSigmas(1:3, seedID) + ...
%                                          reshape(ILab(i,j,:),3,1);
%             segmentSigmas(4:5, seedID) = segmentSigmas(4:5, seedID) + ...
%                                          [j, i]';
%             segmentSizes(seedID) = segmentSizes(seedID) + 1;
%         end
%     end
% end
% 
% seeds(:, segmentSizes>0) = segmentSigmas(:, segmentSizes>0)./repmat(segmentSizes(segmentSizes>0)',size(segmentSigmas,1),1);