function USaccuracy = computeUS(gtLabel, resultLabel)

numGTLabel = length(unique(gtLabel));
leakTotal = 0;

for i = 1:numGTLabel
    gtsup = (gtLabel == i);
    overlapRegion = resultLabel(gtsup);
    overlapLabel = unique(overlapRegion);
    for j = 1:length(overlapLabel)
        sizeOfSeg = find(resultLabel == overlapLabel(j));
	overlapSeg = find(overlapRegion == overlapLabel(j));
	leakSeg = length(sizeOfSeg) - length(overlapSeg);
	leakTotal = leakTotal + leakSeg;
    end
end

USaccuracy = leakTotal/(size(gtLabel,1)*size(gtLabel,2));
