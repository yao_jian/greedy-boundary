function acc = evaluatePerformance(gtLabel, resultLabel)

if ~iscell(gtLabel)
    gtLabel = num2cell(gtLabel, [1,2]);
end

underSeg = 0;
boundaryRecall = 0;
ASA = 0;
CUS = 0;
BL = 0;
for i = 1:length(gtLabel)
    gtSeg = gtLabel{i}.Segmentation;
    gtBoundary = gtLabel{i}.Boundaries;   % It has to be a logical array
    underSeg = underSeg + computeUS(gtSeg, resultLabel);
    CUS = CUS + computeCorrectedUS(gtSeg, resultLabel);
    [dBR, dBL] = computeBoundaryRecall(gtBoundary, resultLabel, 2);
    boundaryRecall = boundaryRecall + dBR;
    BL = BL + dBL;
    ASA = ASA + computeASA(gtSeg, resultLabel);
end

acc.underSeg = underSeg/length(gtLabel);
acc.boundaryRecall = boundaryRecall/length(gtLabel);
acc.ASA = ASA/length(gtLabel);
acc.CUS = CUS/length(gtLabel);
acc.BL = BL/length(BL);
