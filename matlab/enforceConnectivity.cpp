#include <mex.h>
#include <matrix.h>
#include <sstream>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include "SuperpixelLabel.h"

using namespace std;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    const double* labels = mxGetPr(prhs[0]);
    size_t height = mxGetM(prhs[0]);
    size_t width = mxGetN(prhs[0]);
    int imageSize = width*height;
    int seedTotal = (int)mxGetScalar(prhs[1]);

    double* copyLabels = new double[imageSize];
    for (int i=0; i<imageSize; i++)
    {
	copyLabels[i] = labels[i];
    }

    SuperpixelLabel superpixel(copyLabels, height, width, seedTotal);
    superpixel.enforceConnectivity();
    const double* newLabels = superpixel.getLabels();
    
    plhs[0] = mxCreateDoubleMatrix(height, width, mxREAL);
    double* outMatrix = mxGetPr(plhs[0]);
    for (int i = 0; i < imageSize; i++)
    {
	outMatrix[i] = newLabels[i];
    }
    delete[] copyLabels;
}
