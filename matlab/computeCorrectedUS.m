function CUEaccuracy = computeCorrectedUS(gtLabel, resultLabel)

setResultLabel = unique(resultLabel);
numResultLabel = length(setResultLabel);
setGTLabel = unique(gtLabel);
leakTotal = 0;

for i = 1:numResultLabel
    rsup = (resultLabel == setResultLabel(i));
    overlapRegion = gtLabel(rsup);
    n = histc(overlapRegion, setGTLabel);
    leakTotal = leakTotal + abs(sum(n) - max(n));
end

CUEaccuracy = leakTotal/(size(gtLabel,1)*size(gtLabel,2));