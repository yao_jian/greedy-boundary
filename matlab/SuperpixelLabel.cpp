#include "SuperpixelLabel.h"

const int fourNeighborTotal = 4;
const int fourNeighborOffsetX[4] = {-1, 0, 1, 0};
const int fourNeighborOffsetY[4] = { 0,-1, 0, 1};

SuperpixelLabel::SuperpixelLabel(double* inMatrix, int h, int w, int numSeeds):labels(inMatrix), height(h), width(w), seedTotal(numSeeds)
{  
}

SuperpixelLabel::SuperpixelLabel(const SuperpixelLabel & object):height(object.height), width(object.width), seedTotal(object.seedTotal)
{
    double *p = new double[object.width*object.height];
    for (int i = 0; i<object.width*object.height; i++)
    {
	p[i] = object.labels[i];
    }
    labels = p;
}

//SuperpixelLabel::~SuperpixelLabel()
//{
//    delete[] labels;
//}

SuperpixelLabel& SuperpixelLabel::operator=(const SuperpixelLabel & other)
{
    if(&other != this)
    {
        double *p = new double[other.width*other.height];
    	for (int i = 0; i<other.width*other.height; i++)
    	{
	    p[i] = other.labels[i];
    	}
    	labels = p;
        height = other.height;
        width = other.width;
    }
    return *this;
}


void SuperpixelLabel::enforceConnectivity() {
    int imageSize = width*height;
    
    const int minimumSegmentSizeThreshold = imageSize/seedTotal/4;
    
    double* newLabels = new double[imageSize];
    for (int i = 0; i<imageSize; i++)
    {
	newLabels[i] = -1;
    }

    int newLabelIndex = 0;
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            if (newLabels[height*x + y] >= 0) continue;
            
            newLabels[height*x + y] = newLabelIndex;
            
            int adjacentLabel = 0;
            for (int neighborIndex = 0; neighborIndex < fourNeighborTotal; ++neighborIndex) {
                int neighborX = x + fourNeighborOffsetX[neighborIndex];
                int neighborY = y + fourNeighborOffsetY[neighborIndex];
                if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;
                
                if (newLabels[height*neighborX + neighborY] >= 0) adjacentLabel = newLabels[height*neighborX + neighborY];
            }
            
            std::vector<int> connectedXs(1);
            std::vector<int> connectedYs(1);
            connectedXs[0] = x;
            connectedYs[0] = y;
            labelConnectedPixels(x, y, newLabelIndex, newLabels, connectedXs, connectedYs);
            
            int segmentPixelTotal = static_cast<int>(connectedXs.size());
            if (segmentPixelTotal <= minimumSegmentSizeThreshold) {
                for (int i = 0; i < segmentPixelTotal; ++i) {
                    newLabels[height*connectedXs[i] + connectedYs[i]] = adjacentLabel;
                }
                --newLabelIndex;
            }
            
            ++newLabelIndex;
        }
    }
    
    for (int i = 0; i<imageSize; i++)
    {
	labels[i] = newLabels[i];
    }
    delete[] newLabels;    
    seedTotal = newLabelIndex;

}

void SuperpixelLabel::labelConnectedPixels(const int x, const int y, const int newLabelIndex, double* newLabels, std::vector<int>& connectedXs, std::vector<int>& connectedYs) const
{
    int originalLabelIndex = labels[height*x + y];
    for (int neighborIndex = 0; neighborIndex < fourNeighborTotal; ++neighborIndex) {
        int neighborX = x + fourNeighborOffsetX[neighborIndex];
        int neighborY = y + fourNeighborOffsetY[neighborIndex];
        if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;
        
        if (newLabels[height*neighborX + neighborY] < 0 && labels[height*neighborX + neighborY] == originalLabelIndex) {
            connectedXs.push_back(neighborX);
            connectedYs.push_back(neighborY);
            newLabels[height*neighborX + neighborY] = newLabelIndex;
            labelConnectedPixels(neighborX, neighborY, newLabelIndex, newLabels, connectedXs, connectedYs);
        }
    }
}

