function [USsup, BRsup, ASAsup, CUSsup, BLsup] = testingBoundarySlic(numSuperpixelArray, edgeWeight, prefix)

visualize = 0;

GT_PATH = '../data/groundTruth/test';
IMAGE_PATH = '../data/images/test';
RESULT_PATH = fullfile('../data/result', prefix);
if ~exist(RESULT_PATH, 'dir')
    mkdir(RESULT_PATH);
end
imlist = dir(fullfile(IMAGE_PATH, '*.png'));

US = zeros(1, length(imlist)); 
BR = zeros(1, length(imlist)); 
ASA = zeros(1, length(imlist));
CUS = zeros(1, length(imlist));
BL = zeros(1, length(imlist));
USsup = []; BRsup = []; ASAsup = []; CUSsup = []; BLsup = [];
for numSup = numSuperpixelArray
    for i = 1:length(imlist)
        if mod(i,50) == 0
            fprintf('numSup = %d, %d images are processed\n', numSup, i);
        end
        imname = imlist(i).name;
        I = imread(fullfile(IMAGE_PATH, imname));
        assignment = boundarySLIC(I, numSup, edgeWeight);
        save(fullfile(RESULT_PATH, strrep(imname, '.png', sprintf('_%d_SLIC.mat',numSup))), 'assignment');
        gtFile = load(fullfile(GT_PATH, [imname(1:end-3) 'mat']));
        gt = gtFile.groundTruth;
        acc = evaluatePerformance(gt, assignment.labels);
        US(i) = acc.underSeg;
        BR(i) = acc.boundaryRecall;
        ASA(i) = acc.ASA;
        CUS(i) = acc.CUS;
        BL(i) = acc.BL;
        if visualize
            drawBoundary(assignment.labels, I, 1); %#ok<UNRCH>
        end
    end
    
    USsup = [USsup mean(US)]; %#ok<*AGROW>
    BRsup = [BRsup mean(BR)];
    ASAsup = [ASAsup mean(ASA)];
    CUSsup = [CUSsup mean(CUS)];
    BLsup = [BLsup, mean(BL)];
end

save(fullfile(RESULT_PATH, 'acc_BSD.mat'), 'USsup', 'BRsup', 'ASAsup', 'CUSsup', 'BLsup');


