#include<mex.h>
#include<cmath>

struct pointArray {
    int * point_ptr;
    int numPoints;
public:
    ~pointArray(){
        delete[] point_ptr;
    }
    
    pointArray(){}
    pointArray(const pointArray& object):numPoints(object.numPoints){
        int* p = new int[numPoints*2];
        for (int i = 0; i<object.numPoints; i++){
            point_ptr[i] = object.point_ptr[i];
            point_ptr[i+numPoints] = object.point_ptr[i+numPoints];
        }
    }    
    
	
};

pointArray detPointOnLine(const int*, const int*);

void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
    if (nrhs != 4)
    {
	mexErrMsgTxt("Proper Inputs Required!!");
    }

    double* xArray = mxGetPr(prhs[0]);
    double* yArray = mxGetPr(prhs[1]);
    double* refPoint = mxGetPr(prhs[2]);
    double* LabEdge = mxGetPr(prhs[3]);
    size_t height = mxGetM(prhs[0]);
    size_t width = mxGetN(prhs[0]);
    size_t Iheight = mxGetM(prhs[3]);
    size_t Iwidth = mxGetN(prhs[3]);

    plhs[0] = mxCreateDoubleMatrix(height, width, mxREAL);
    double* edgeInfo = mxGetPr(plhs[0]);
    int* p = new int[2];
    int* r = new int[2];
    for (int i=0; i<height; i++){
        for (int j=0; j<width; j++){
            p[0] = xArray[j*height+i];
            p[1] = yArray[j*height+i];
            r[0] = (int)refPoint[0];
            r[1] = (int)refPoint[1];
            pointArray points = detPointOnLine(p, r);
            double maxEdgeValue = 0;
            for (int k=1; k<points.numPoints; k++){
                int rowInd = points.point_ptr[k+points.numPoints];
                int colInd = points.point_ptr[k];
                if (LabEdge[colInd*Iheight+rowInd]>maxEdgeValue)
                    maxEdgeValue = LabEdge[colInd*Iheight+rowInd];
            }
            edgeInfo[j*height+i] = maxEdgeValue;
        }
    }
    delete[] p;
    delete[] r;
}


pointArray detPointOnLine(const int* point0, const int* point1)
{
    int x0 = point0[0];
    int y0 = point0[1];
    int x1 = point1[0];
    int y1 = point1[1];
    
    bool steep = (std::abs(y1-y0) > std::abs(x1-x0));
    if (steep)
    {
        int temp = x0;
        x0 = y0;
        y0 = temp;
        
        temp = x1;
        x1 = y1;
        y1 = temp;
    }
    
    int deltax = std::abs(x1-x0);
    int deltay = std::abs(y1-y0);
    int error = deltax/2;
    int ystep;
    int y = y0;
    
    if (y0<y1)
        ystep = 1;
    else
        ystep = -1;
    
    int inc;
    if (x0<x1)
        inc = 1;
    else
        inc = -1;
    
    //mxArray* points = plhs[0] = mxCreateDoubleMatrix(deltax+1, 2, mxREAL);
    int* points_ptr = new int[(deltax+1)*2];
    int accumulator = 0;
    int x = x0;
    while (accumulator < deltax+1)
    {
        if (steep)
        {
            points_ptr[accumulator] = y;
            points_ptr[accumulator+deltax+1] = x;
        }
        else
        {
            points_ptr[accumulator] = x;
            points_ptr[accumulator+deltax+1] = y;
        }
        error = error - deltay;
        if (error<0)
        {
            y = y + ystep;
            error = error + deltax;
        }
        x = x + inc;
        accumulator = accumulator + 1;  
    }
    
    pointArray returnValue;
    returnValue.point_ptr = points_ptr;
    returnValue.numPoints = deltax+1;
        
    return(returnValue);
}
