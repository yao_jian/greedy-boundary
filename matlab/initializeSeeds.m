function seeds = initializeSeeds(ILab, LabEdges, superpixelTotal)

GlobalVar;

NeighborOffsetX = [-1 -1 0 1 1 1 0 -1];
NeighborOffsetY = [0 -1 -1 -1 0 1 1 1];

height = size(ILab, 1);
width = size(ILab, 2);
imageSize = height * width;

gridSize = sqrt(imageSize/superpixelTotal);
offsetX = floor(gridSize/2);
offsetY = floor(gridSize/2);

%Ypos = floor(offsetY+0.5 : gridSize : height-10^-5);
%Xpos = floor(offsetX+0.5 : gridSize : width-10^-5);
Ypos = 1+round(offsetY : gridSize : height-1);
Xpos = 1+round(offsetX : gridSize : width-1);

seeds = zeros(NumFeatures, length(Xpos)*length(Ypos)); % the order is in row-wise
for i = 1:size(seeds,2)
    rowindex = Ypos(ceil(i/length(Xpos)));
    if mod(i, length(Xpos)) == 0
        colindex = Xpos(end);
    else
        colindex = Xpos(mod(i, length(Xpos)));
    end
    seeds(1:3,i) = ILab(rowindex, colindex, :);
    seeds(4:5,i) = [colindex rowindex];
    
    localEdgeMin = LabEdges(rowindex, colindex);
    for j = 1:length(NeighborOffsetX)
        neighborRowIndex = rowindex + NeighborOffsetY(j);
        neighborColIndex = colindex + NeighborOffsetX(j);
        if neighborRowIndex<1 || neighborRowIndex>height || ...
           neighborColIndex<1 || neighborColIndex>width
            continue;
        end
        if LabEdges(neighborRowIndex, neighborColIndex) < localEdgeMin
            localEdgeMin =  LabEdges(neighborRowIndex, neighborColIndex);
            minR = neighborRowIndex;
            minC = neighborColIndex;
        end
    end
    if localEdgeMin ~= LabEdges(rowindex, colindex)
        seeds(1:3,i) = ILab(minR, minC, :);
        seeds(4:5,i) = [minC, minR];
    end
end